# dtraceify 

Add dtrace probes to your code automatically. 

dtracify recursively analyses your module with all the dependencies and inserts probe code.
It also generates a dscript that you can then use to listen for those probes. 

Inspired by feedback from my Great British Node Conf Talk 
https://www.youtube.com/watch?v=_g6Z_AMCylg 
https://github.com/rvagg/node-levelup/pull/197

## Usage 

```bash
$ npm install dtraceify -g 
```

cd into your project folder.

``` bash
$ dtraceify yourentrypoint.js
$ node yourentrypoint.js
$ dtrace yourentrypoint.d
```





